import { createRouter, createWebHistory } from 'vue-router'
import Home from './pages/Home.vue'
import TestPage from './pages/TestPage.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/test',
    name: 'TestPage',
    component: TestPage
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes
})
export default router